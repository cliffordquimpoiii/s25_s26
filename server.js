//[SECTION] Concept of Separation of Concerns

   //==> In computer science, (SoC) is a "DESIGN PRINCIPLE" for separating a computer program into distinct sections. 

   //=> Each section will address a separate concern, function, or a set of information that affects the code of a computer program. 

   //Benefits of following this principle:
      //=> isolation of responsibilities
//TASK: Start implementing *routes* 
   //register  ==> '/register'
   //login     ==> '/login'
   //profile   ==> '/profile'
   //home      ==> '/' (entry point path of the domain)
   //HOW? 
const http = require('http'); 
    
let port = 4000; 


http.createServer((request, response) => {

   //console.log(request); //object
   //url property 
   //create a control structure
   if (request.url === '/') {
      response.write(`Welcome to the Home Page`);
      response.end(); 
   } else if (request.url === '/register') {
      response.write(`Welcome to the Register Page`);
      response.end() //to identify the end of transmission
   } else if (request.url === '/login') {
      response.write(`Welcome to the Login Page`); 
      response.end(); 
   } else if (request.url === '/profile') {
      response.write(`Welcome to the Profile Page`);  
      response.end(); 
   } else {
      response.write('Page Not Found');
      response.end()
   }
   
}).listen(port); 

console.log(`Server of B165 is on Port ${port}`); 
